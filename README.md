# Agbot Sim Gazebo Setup Tutorial

The AgBot Sim package allows for users to simulate autonomously driving a UTV in Gazebo. This is accomplished by using Gazebo Models and Plugins from [osrf/drcsim](https://bitbucket.org/osrf/drcsim/src/default/).

### **Prerequisits : Install ROS Indigo**

### **Install Gazebo 7:**
Install Gazebo 7 by following the "using a specific Gazebo version with ROS" [this](http://gazebosim.org/tutorials/?tut=ros_wrapper_versions) tutorial.

### **Install DRC Sim From Source:**

Install DRC Sim from source in your standard catkin workstation by following [this](http://gazebosim.org/tutorials?tut=drcsim_install#Compilingfromsource) tutorial. 

**On Step 3, do not make a temporary workstation, simply change to the {your catkin ws}/src and download the required software there.**

**NOTES**
1. If the software will not make and install because the *tinyxml2* dependency is missing, execute the following to fix the dependency. 
```sh
sudo apt-get install libtinyxml2-dev
```

2. If you are unable to cloan the required software by using *hg*, you will have to manually download it, and copy it into your {your catkin ws}/src directory. 

### **Permanatly Source DRC Sim:**

To avoid having to manually source the DRC Sim setup every time you open a new terminal, execute the following. 

```sh
echo "source {your catkin ws}/install/setup.bash" >> ~/.bashrc
echo "source {your catkin ws}/install/share/drcsim/setup.sh" >> ~/.bashrc
```

### **Clone and Install AgBot Sim:**
Clone AgBot Sim into your catkin workstation and install it by executing the following:
```sh
cd {your catkin ws}/src
git clone https://timallenyi111@bitbucket.org/iupui_agbot/agbot-sim.git
cd ..
catkin_make install
```

### **Test Configuration**
You should now be able to launch the gazebo simulator with an XP900 UTV that is controllable via ROS messages. To test that everything is working execute the following:

```sh
VRC_CHEATS_ENABLED=1 roslaunch agbot-sim utv_empty.launch
```

After the simulator has started, open up another terminal and execute the following:

```sh
rostopic list
```
The list of topics should include:
```sh
...
/drc_vehicle_xp900/brake_pedal/cmd
/drc_vehicle_xp900/brake_pedal/state
/drc_vehicle_xp900/direction/cmd
/drc_vehicle_xp900/direction/state
/drc_vehicle_xp900/gas_pedal/cmd
/drc_vehicle_xp900/gas_pedal/state
/drc_vehicle_xp900/hand_brake/cmd
/drc_vehicle_xp900/hand_brake/state
/drc_vehicle_xp900/hand_wheel/cmd
/drc_vehicle_xp900/hand_wheel/state
/drc_vehicle_xp900/key/cmd
/drc_vehicle_xp900/key/state
...
```

To learn more about what each of these topics does, see [this](http://gazebosim.org/tutorials?tut=drcsim_vehicle&cat=drcsim#InstallDRCSimulator) tutorial.