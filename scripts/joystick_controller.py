#!/usr/bin/env python
import rospy
from gazebo_msgs.srv import ApplyJointEffort,JointRequest
from std_msgs.msg import Float64
from std_msgs.msg import String
from sensor_msgs.msg import Joy
from std_msgs.msg import Bool

def startUp():
    rospy.init_node('joystic_controller')

    global brakePub
    brakePub = rospy.Publisher("/drc_vehicle_xp900/brake_pedal/cmd",Float64,queue_size=10)
    global throttleSetPub
    throttleSetPub = rospy.Publisher("/drc_vehicle_xp900/gas_pedal/cmd",Float64,queue_size=10)
    global steeringSetPub
    steeringSetPub = rospy.Publisher("/drc_vehicle_xp900/hand_wheel/cmd",Float64,queue_size=10)
    global handBrakeState
    global button2Status
    global handBrakePub
    handBrakePub = rospy.Publisher("/drc_vehicle_xp900/hand_brake/cmd",Float64,queue_size=10)

    joySub = rospy.Subscriber('/joy',Joy,joyCallback)
    handBrakeSub = rospy.Subscriber('/drc_vehicle_xp900/hand_brake/state',Float64,handBrakeCallback)

    button2Status = False

    print("node started")
    rospy.spin()


def handBrakeCallback(data):
    global handBrakeState
    handBrakeState = data.data


def joyCallback(data):
    global throttleControlPub
    global brakePub
    global steeringSetPub
    global handBrakeState
    global button2Status
    global handBrakePub

    rt = data.axes[5]
    lt = data.axes[2]
    ls = data.axes[0]
    bBut = data.buttons[1]

    if lt < 0:

        brakePub.publish(True)
        throttleSetPub.publish(0)
        #print(lt)
    else:
        rt = 1 - rt
        print("Throttle")
        print(rt)
        brakePub.publish(False)
        throttleSetPub.publish(rt*0.5)

    sSetPoint = ls*3
    steeringSetPub.publish(sSetPoint)

    if bBut == 1 and button2Status == False:
        if handBrakeState == 1.0:
            handBrakePub.publish(0)
        else:
            handBrakePub.publish(1.0)

        button2Status = True

    if bBut == 0:
        button2Status = False

    print button2Status



if __name__=='__main__':
    try:
        startUp()
    except rospy.ROSInterruptException:
        pass
